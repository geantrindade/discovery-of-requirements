package br.edu.unipampa.ontology;

import java.util.Set;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLLiteral;

public abstract class OntoEntity {
    
    public OntoModel ontoModel;
    
    protected OntoEntity() {
        ontoModel = OntoModel.getInstance("C:/Users/Douglas/Google Drive/Bacharelado/6° Semestre/Desenvolvimento Web Semântica/OWL API/analise.owl", "pt");
    }
    
    protected String getIdentifier(OWLEntity entity) {
        return entity.toStringID();
    }
    
    protected String getLabel(OWLEntity entity) {
        String label = null;
        Set<OWLAnnotation> annotationSet = entity.getAnnotations(getOntoModel().getOntology());
        for (OWLAnnotation annotation : annotationSet) {
            if (annotation.containsEntityInSignature(getOntoModel().getFactory().getRDFSLabel())) {
                if (annotation.getValue() instanceof OWLLiteral) {
                    OWLLiteral value = (OWLLiteral) annotation.getValue();
                    if (value.hasLang(getOntoModel().getLanguage())) {
                        label = value.getLiteral();
                    }
                }
            }
        }
        return label;
    }
    
    protected String getComment(OWLEntity entity) {
        String comment = null;
        Set<OWLAnnotation> annotationSet = entity.getAnnotations(getOntoModel().getOntology());
        for (OWLAnnotation annotation : annotationSet) {
            if (annotation.containsEntityInSignature(getOntoModel().getFactory().getRDFSComment())) {
                if (annotation.getValue() instanceof OWLLiteral) {
                    OWLLiteral value = (OWLLiteral) annotation.getValue();
                    if (value.hasLang(getOntoModel().getLanguage())) {
                        comment = value.getLiteral();
                    }
                }
            }
        }
        return comment;
    }

    /**
     * @return the ontoModel
     */
    public OntoModel getOntoModel() {
        return ontoModel;
    }
    
}