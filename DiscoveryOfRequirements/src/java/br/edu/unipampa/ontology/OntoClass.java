package br.edu.unipampa.ontology;

import br.edu.unipampa.model.Input;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.Node;

public class OntoClass extends OntoEntity {

    private OWLClass owlClass;

    public OntoClass() {
        super();
        owlClass = ontoModel.getFactory().getOWLThing();
    }

    public OntoClass(String id) {
        super();
        IRI iri = IRI.create(id);
        owlClass = ontoModel.getFactory().getOWLClass(iri);
    }

    public OntoClass(OWLClass cls) {
        super();
        owlClass = cls;
    }

    public String getIdentifier() {
        return getIdentifier(owlClass);
    }

    public String getLabel() {
        return getLabel(owlClass);
    }

    public String getComment() {
        return getComment(owlClass);
    }

    public List<OntoClass> getSuperClasses() {
        if (!owlClass.isTopEntity()) {
            List<OntoClass> classList = new ArrayList<OntoClass>();
            Set<Node<OWLClass>> superClasses = getOntoModel().getReasoner().getSuperClasses(owlClass, true).getNodes();
            for (Node<OWLClass> node : superClasses) {
                String key = node.getRepresentativeElement().toStringID();
                classList.add(new OntoClass(key));
            }
            return classList;
        } else {
            return null;
        }
    }

    public List<OntoClass> getSubClasses() {
        if (!owlClass.isBottomEntity()) {
            List<OntoClass> classList = new ArrayList<OntoClass>();
            Set<Node<OWLClass>> subClasses = getOntoModel().getReasoner().getSubClasses(owlClass, true).getNodes();
            for (Node<OWLClass> node : subClasses) {
                String key = node.getRepresentativeElement().toStringID();
                classList.add(new OntoClass(key));
            }
            return classList;
        } else {
            return null;
        }
    }

    public List<OntoIndividual> getIndividuals() {
        List<OntoIndividual> individualList = new ArrayList<OntoIndividual>();
        Set<OWLIndividual> individuals = owlClass.getIndividuals(getOntoModel().getOntology());
        for (OWLIndividual ind : individuals) {
            individualList.add(new OntoIndividual(ind));
        }
        return individualList;
    }

    public OWLClass getOWL() {
        return owlClass;
    }

    public ArrayList<Input> getPropriedadesObjeto() {
        OWLOntology o = ontoModel.getOntology();
        ArrayList<Input> inputs = new ArrayList<Input>();

        for (OWLObjectProperty prop : o.getObjectPropertiesInSignature()) {
            for (OWLClassExpression classe : prop.getDomains(o)) {
                System.out.println(this.owlClass.toString());
                System.out.println(classe.toString());
                if (classe.toString().equals(this.owlClass.toString())) {
                    if (classe.asOWLClass().getSubClasses(o).isEmpty()) {
                        inputs.add(new Input(prop.toString(), "text"));
                    }
                }
            }
        }

        return inputs;
    }
    
    public ArrayList<Input> getPropriedadesDado() {
        OWLOntology o = ontoModel.getOntology();
        ArrayList<Input> inputs = new ArrayList<Input>();

        for (OWLDataProperty prop : o.getDataPropertiesInSignature()) {
            for (OWLClassExpression classe : prop.getDomains(o)) {
                if (classe.toString().equals(this.owlClass.toString())) {
                    inputs.add(new Input(prop.toString(), "text"));
                }
            }
        }

        return inputs;
    }
}
