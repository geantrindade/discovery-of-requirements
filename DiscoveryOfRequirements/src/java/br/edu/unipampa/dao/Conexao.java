/**
 * Classe contendo informações que são necessarias para a construção de uma
 * conexão
 */
package br.edu.unipampa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 * @author Douglas Giordano
 * @since 28/04/2013
 */
public class Conexao {

    private String nome;
    private String banco;
    private String local;
    private int porta;
    private String usuario;
    private String senha;
    private String url;

    public Conexao() {
    }

    public Conexao(String nome, String banco, String local, int porta, String usuario, String senha) {
        this.nome = nome;
        this.banco = banco;
        this.local = local;
        this.porta = porta;
        this.usuario = usuario;
        this.senha = senha;
    }

    public String getBanco() {
        return banco;
    }

    public int getPorta() {
        return porta;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getSenha() {
        return senha;
    }

    public String getUrlSQLServer() {
        url = "jdbc:mysql://" + getLocal() + "/" + banco;
        return url;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public void setPorta(int porta) {
        this.porta = porta;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Verifica se o tipo int deve ir nulo para o banco de dados
     *
     * @return numero tratado
     */
    public static String verificaNull(int numero) {
        if (numero != 0) {
            return numero + "";
        } else {
            return null;
        }

    }

    /**
     * Verifica se o tipo String deve ir nulo para o banco de dados
     *
     * @return string tratada
     */
    public static String verificaNull(String string) {
        if (string != null) {
            if (!string.equals("")) {
                return "'" + string + "'";
            } else {
                return "NULL";
            }
        } else {
            return "NULL";
        }
    }

    /**
     * Verifica se o tipo Date deve ir nulo para o banco de dados
     *
     * @return string tratada
     */
    public static String verificaNull(Date date) {
        if (date != null) {
            return "'";
        } else {
            return "NULL";
        }
    }

    public boolean testaConexao() {
        boolean conectou;
        Connection con = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(getUrlSQLServer(), getUsuario(), getSenha());
            conectou = true;
            con.close();
        } catch (ClassNotFoundException erro) {
            JOptionPane.showMessageDialog(null, "Arquivo não encontrado: " + erro);
            conectou = false;
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, "Problemas ao conectar no SQL: " + erro);
            conectou = false;
        }
        return conectou;
    }
}
