/*
 * Conexão com o banco de dados SQL
 */
package br.edu.unipampa.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 * @author Douglas Giordano
 * @since 28/04/2013
 */
public class ConexaoMySQL {

    public static Connection getCon() {
        return con;
    }

    public static void setCon(Connection aCon) {
        con = aCon;
    }
    private static Connection con = null;
    private PreparedStatement smt;
    private ResultSet rs;

    public ConexaoMySQL() { 
        conectar();
    }

    public Connection conectar() {
            Conexao conexao = null;
        try {
            conexao = new br.edu.unipampa.dao.Conexao("Conexao", "requisitoOntologia", "localhost", 3306, "root", "root");
            if (getCon() == null) {
                setCon(DriverManager.getConnection(conexao.getUrlSQLServer(), conexao.getUsuario(), conexao.getSenha()));

            } else if (getCon().isClosed()) {
                setCon(DriverManager.getConnection(conexao.getUrlSQLServer(), conexao.getUsuario(), conexao.getSenha()));
            }
        }  catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, erro);
        }
        return getCon();
    }

    public void desconectar() {
        try {
            if (smt != null) {
                smt.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException erro) {
            System.out.println("Ocorreu um erro " + erro);
        }
    }

    public ResultSet select(String script) {
        try {
            smt = getCon().prepareStatement(script);
            rs = smt.executeQuery();

        } catch (SQLException erro) {
            System.out.println(erro);
        }
        return rs;
    }

    public boolean salvar(String script) {
        boolean sucesso = false;
        try {
            smt = getCon().prepareStatement(script);
            smt.executeUpdate();
            sucesso = true;

        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, erro);
        } finally {
            desconectar();
        }
        return sucesso;
    }

    public boolean deletar(String script) {
        boolean sucesso = false;
        try {
            smt = getCon().prepareStatement(script);
            smt.execute();
            sucesso = true;

        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null,"Você não poderá excluir este registro, \n pois o mesmo possui alguma relação com outro cadastro!");
        } finally {
            desconectar();
        }
        return sucesso;
    }

    public boolean executar(String script) {
        boolean sucesso = false;
        try {
            smt = getCon().prepareStatement(script);
            smt.execute();
            sucesso = true;

        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, erro);
        } finally {
            desconectar();
        }
        return sucesso;
    }

        public static int proximoCodigo(String campo, String tabela) {
        ConexaoMySQL conexao = new ConexaoMySQL();
        ResultSet rs;
        int ultimoCodigo = 0;

        try {
            rs = conexao.select("SELECT max(" + campo + ") as MAX2 FROM " + tabela);
            while (rs.next()) {
                ultimoCodigo = rs.getInt("MAX2") + 1;
            }
        } catch (SQLException erro) {
            JOptionPane.showMessageDialog(null, erro);
        } finally {
            conexao.desconectar();
        }
        return ultimoCodigo;
    }

}
