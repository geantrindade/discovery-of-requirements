/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.model;

/**
 *
 * @author Douglas
 */
public class Input {
    private String nomeInput;
    private String tipoInput;
    
    public Input(String nome, String tipo){
        this.nomeInput = nome;
        this.tipoInput = tipo;
    }

    /**
     * @return the nomeInput
     */
    public String getNomeInput() {
        return nomeInput;
    }

    /**
     * @param nomeInput the nomeInput to set
     */
    public void setNomeInput(String nomeInput) {
        this.nomeInput = nomeInput;
    }

    /**
     * @return the tipoInput
     */
    public String getTipoInput() {
        return tipoInput;
    }

    /**
     * @param tipoInput the tipoInput to set
     */
    public void setTipoInput(String tipoInput) {
        this.tipoInput = tipoInput;
    }
}
