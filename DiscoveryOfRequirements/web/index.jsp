<%-- 
    Document   : index
    Created on : 03/02/2015, 23:12:09
    Author     : Douglas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Requisitos</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-default  navbar-fixed-top  navbar-inverse">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/GerenciadorRequisitosWazlawick">Requisitos</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="/GerenciadorRequisitosWazlawick/RequisitoController?tipoRequisito=Funcional">Funcionais</a></li>
                        <li><a href="/GerenciadorRequisitosWazlawick/RequisitoController?tipoRequisito=NaoFuncional">Não Funcionais</a></li>
                        <li><a href="/GerenciadorRequisitosWazlawick/RequisitoController?tipoRequisito=Suplementar">Suplementares</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="#">Sobre</a>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="container theme-showcase" style="margin-top: 5%">
            <c:if test="${empty inputs}">
                <div class="jumbotron">
                    <div class="container">
                        <h1>Documentação de Requisitos!</h1>
                        <p>Este sistema tem como objetivo possibilitar o cadastro de requisitos segundo Wazlawick.</p>
                    </div>
                </div>
            </c:if>
            <c:if test="${not empty inputs}">
                <form>
                    <div class="well  well-sm">
                        <button type="reset" class="btn btn-default">Novo</button>
                        <button type="button" id="salvar" class="btn btn-success">Salvar</button>
                    </div>
                    <c:forEach items="${inputs}" var="input">
                        <div class="form-group">
                            <label class="nomeCampo"><c:out value="${input.nomeInput}"></c:out></label>
                                <input type="text" class="form-control valorCampo" >
                            </div>
                    </c:forEach>
                </form>
            </c:if>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script type="text/javascript">
            $("#salvar").click(function () {
                var requisito = "";
                for (i = 0; i < $(".nomeCampo").length; i++) {
                    requisito = requisito + "<b>" + $(".nomeCampo")[i].innerHTML + "</b>:  ";
                    requisito = requisito + $(".valorCampo")[i].value + "|||||";
                }
                $.post("/GerenciadorRequisitosWazlawick/SalvarRequisitoController",{requisito: requisito} ,function (data) {
                    alert(data);
                });
            });
        </script>
    </body>
</html>
